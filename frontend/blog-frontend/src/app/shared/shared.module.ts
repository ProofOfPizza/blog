import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MarkdownEditComponent } from './markdown-edit/markdown-edit.component';
import { MarkdownPreviewComponent } from './markdown-preview/markdown-preview.component';



@NgModule({
  declarations: [
    MarkdownEditComponent,
    MarkdownPreviewComponent
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
