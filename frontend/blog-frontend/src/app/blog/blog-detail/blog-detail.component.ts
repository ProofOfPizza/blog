import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Blog } from '@blog/../build/openapi';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { BlogService } from '../blog.service';

@Component({
  selector: 'app-blog-detail',
  templateUrl: './blog-detail.component.html',
  styleUrls: ['./blog-detail.component.scss'],
})
export class BlogDetailComponent implements OnInit {
  id: string = '';
  blog$: Observable<Blog> = new Observable<Blog>();
  error: { code: number; message: string } | null = null;

  constructor(private blogService: BlogService, private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.id = params.id;
    });
    this.blog$ = this.blogService.getBlogById(this.id);
  }

  onClickDelete(): void {
    this.blogService
      .deleteBlog(this.id)
      .pipe(take(1))
      .subscribe(
        (blog) => {
          this.router.navigate(['..']);
          console.log(`deleted blog: ${JSON.stringify(blog)}`);
        },
        (error) => {
          this.error = {
            code: error.code,
            message: error.message,
          };
          console.log({ error });
        },
      );
  }
}

