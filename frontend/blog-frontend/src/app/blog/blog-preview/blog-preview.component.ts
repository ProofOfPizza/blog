import { Component, Input, OnInit } from '@angular/core';
import { Blog } from 'build/openapi';

@Component({
  selector: 'app-blog-preview',
  templateUrl: './blog-preview.component.html',
  styleUrls: ['./blog-preview.component.scss'],
})
export class BlogPreviewComponent implements OnInit {
  @Input('blog') blog!: Blog;

  constructor() {}

  ngOnInit(): void {}
}

