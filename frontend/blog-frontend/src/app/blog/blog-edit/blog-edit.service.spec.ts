import { TestBed } from '@angular/core/testing';

import { BlogEditService } from './blog-edit.service';

describe('BlogEditService', () => {
  let service: BlogEditService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BlogEditService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
