import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { BlogService } from '../blog.service';
import { BlogEditService, CHANGE_TO } from './blog-edit.service';

@Component({
  selector: 'app-blog-edit',
  templateUrl: './blog-edit.component.html',
  styleUrls: ['./blog-edit.component.scss'],
})
export class BlogEditComponent implements OnInit {
  @ViewChild('content') content!: ElementRef;
  @ViewChild('linkUrl') linkUrl!: ElementRef;
  @ViewChild('linkText') linkText!: ElementRef;
  enteringUrl = false;
  id: string = '';
  editMode = false;
  blogForm!: FormGroup;

  constructor(
    private blogService: BlogService,
    private route: ActivatedRoute,
    private router: Router,
    private blogEditservice: BlogEditService,
  ) {
    this.route.params.subscribe((params) => {
      this.id = params.id;
      this.editMode = params.id != null;
      this.initForm();
    });
  }

  ngOnInit(): void {}

  private initForm() {
    let title = '';
    let author = '';
    let content = '';
    let date = '';
    let tags: Array<string> = [];
    const init = () =>
      (this.blogForm = new FormGroup({
        title: new FormControl(title, [Validators.required, Validators.minLength(8)]),
        author: new FormControl(author, [Validators.required, Validators.minLength(3)]),
        content: new FormControl(content, [Validators.required, Validators.minLength(50)]),
        date: new FormControl(date),
        tags: new FormArray(tags.map((t) => new FormControl(t))),
      }));

    if (this.editMode) {
      this.blogService
        .getBlogById(this.id)
        .pipe(take(1))
        .subscribe((blog) => {
          title = blog.title;
          author = blog.author;
          content = blog.content;
          date = blog.date ?? '';
          tags = blog.tags ?? [];
          return init();
        });
    }
    init();
  }

  onSubmit(): void {
    console.log(this.blogForm);
    if (this.editMode) {
      this.blogService
        .updateBlog({ ...this.blogForm.value, id: this.id })
        .pipe(take(1))
        .subscribe((result) => {
          this.router.navigate([`/blog/${result.id}`]);
        });
    } else {
      this.blogService
        .createBlog(this.blogForm.value)
        .pipe(take(1))
        .subscribe((result) => {
          this.router.navigate([`/blog/${result.id}`]);
        });
    }
  }

  onClickBold(): void {
    this.content = this.blogEditservice.addMarkdown(this.content, CHANGE_TO.BOLD);
  }

  onClickItalic(): void {
    this.content = this.blogEditservice.addMarkdown(this.content, CHANGE_TO.ITALIC);
  }

  onClickHeader(): void {
    this.content = this.blogEditservice.addMarkdown(this.content, CHANGE_TO.HEADER);
  }

  onClickInlineCode(): void {
    this.content = this.blogEditservice.addMarkdown(this.content, CHANGE_TO.INLINE_CODE);
  }

  onClickCodeBlock(): void {
    this.content = this.blogEditservice.addMarkdown(this.content, CHANGE_TO.CODE_BLOCK);
  }

  onClickLinkOpen(): void {
    this.enteringUrl = true;

    setTimeout(() => {
      [this.linkText, this.linkUrl] = this.blogEditservice.createLink(this.content, this.linkText, this.linkUrl);
    });
  }

  onClickOrderedList(): void {
    this.blogEditservice.addMarkdown(this.content, CHANGE_TO.ORDERED_LIST);
  }

  onClickUnorderedList(): void {
    this.blogEditservice.addMarkdown(this.content, CHANGE_TO.UNORDERED_LIST);
  }

  onClickLinkSave(): void {
    this.content = this.blogEditservice.saveLink(this.content, this.linkText, this.linkUrl);
    this.enteringUrl = false;
  }

  onClickLinkCancel(): void {
    console.log(this.content);
    this.enteringUrl = false;
  }

  onKeyEnter(event: KeyboardEvent) {
    if (event.key === 'Enter' && this.blogEditservice.listIndentLevel >= 0) {
      this.blogEditservice.addMarkdown(this.content, CHANGE_TO.LIST_ITEM);
    }
  }

  onClickQuote() {
    this.blogEditservice.addMarkdown(this.content, CHANGE_TO.QUOTE);
  }
}

