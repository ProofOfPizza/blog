import { ElementRef, Injectable } from '@angular/core';

export enum CHANGE_TO {
  BOLD = '**',
  CODE_BLOCK = '```',
  HEADER = '## ',
  INLINE_CODE = '``',
  ITALIC = '*',
  LINK = '[]()',
  LIST_ITEM = 'listItem',
  NOTHING = '',
  QUOTE = '>',
  ORDERED_LIST = '1. ',
  UNORDERED_LIST = '- ',
}
@Injectable({
  providedIn: 'root',
})
export class BlogEditService {
  readonly SPACES = '    ';
  listIndentLevel = -1; //-1 no list 0 and up are list levels
  listType: Array<CHANGE_TO.ORDERED_LIST | CHANGE_TO.UNORDERED_LIST> = [];

  addMarkdown(textarea: ElementRef, change: CHANGE_TO): ElementRef {
    let { begin, end, before, selection, after, changeTo } = this.getTextPartition(textarea, change);

    switch (changeTo) {
      case CHANGE_TO.BOLD:
      case CHANGE_TO.ITALIC:
      case CHANGE_TO.INLINE_CODE:
        [selection, end] = this.changeSimple(selection, changeTo, begin, end);
        break;
      case CHANGE_TO.HEADER:
        [selection, end] = this.changeHeader(selection, end);
        break;
      case CHANGE_TO.CODE_BLOCK:
        [before, selection, after, end] = this.changeCodeBlock(before, selection, after, end);
        break;
      case CHANGE_TO.ORDERED_LIST:
      case CHANGE_TO.UNORDERED_LIST:
        // for type inference ...
        change = change === CHANGE_TO.UNORDERED_LIST ? CHANGE_TO.UNORDERED_LIST : CHANGE_TO.ORDERED_LIST;
        [before, selection, after, end] = this.changeList(before, selection, after, end, change);
        break;
      case CHANGE_TO.LIST_ITEM:
        [before, selection, end] = this.listItemNext(before, selection, end);
        break;
      case CHANGE_TO.QUOTE:
        [selection, end] = this.addQuote(selection, end);
    }

    const textAfter = ''.concat(before, selection, after);
    textarea.nativeElement.value = textAfter;
    textarea.nativeElement.selectionEnd = end;
    textarea.nativeElement.focus();
    return textarea;
  }

  createLink(textarea: ElementRef, modalText: ElementRef, modalUrl: ElementRef): [ElementRef, ElementRef] {
    let { selection } = this.getTextPartition(textarea);
    const regexp: RegExp = new RegExp('^((https://)|(http://)|(www.)).*');

    if (selection.match(regexp)) {
      modalUrl.nativeElement.value = selection;
      modalText.nativeElement.focus();
    } else {
      modalText.nativeElement.value = selection;
      modalUrl.nativeElement.focus();
    }

    return [modalText, modalUrl];
  }

  saveLink(textarea: ElementRef, modalText: ElementRef, modalUrl: ElementRef): ElementRef {
    let { before, after } = this.getTextPartition(textarea);
    const linkText = modalText.nativeElement.value;
    const linkUrl = modalUrl.nativeElement.value;
    const selection = `[${linkText}](${linkUrl})`;
    textarea.nativeElement.value = ''.concat(before, selection, after);
    textarea.nativeElement.focus();
    return textarea;
  }

  private getTextPartition(
    textarea: ElementRef,
    changeTo?: CHANGE_TO,
  ): {
    begin: number;
    end: number;
    before: string;
    selection: string;
    after: string;
    changeTo: CHANGE_TO;
  } {
    const begin = textarea.nativeElement.selectionStart;
    let end = textarea.nativeElement.selectionEnd;
    let textBefore = textarea.nativeElement.value;
    let before = textBefore.slice(0, begin);
    let selection = textBefore.slice(begin, end);
    let after = textBefore.slice(end);

    // see if te addition is there meaning we should undo it
    if (changeTo === CHANGE_TO.BOLD || changeTo === CHANGE_TO.INLINE_CODE || changeTo === CHANGE_TO.ITALIC) {
      if (changeTo && before.endsWith(changeTo) && after.startsWith(changeTo)) {
        const chars = changeTo.length;
        before = before.slice(0, -chars);
        after = after.slice(chars);
        changeTo = CHANGE_TO.NOTHING;
      }
    } else {
      if (changeTo && changeTo === CHANGE_TO.HEADER && before.endsWith(changeTo)) {
        const chars = changeTo.length;
        before = before.slice(0, -chars);
        changeTo = CHANGE_TO.NOTHING;
      }
    }
    //in case of non-simple additions:
    changeTo = changeTo ?? CHANGE_TO.NOTHING;

    return {
      begin,
      end,
      before,
      selection,
      after,
      changeTo,
    };
  }

  private changeSimple(selection: string, changeTo: CHANGE_TO, begin: number, end: number): [string, number] {
    selection = `${changeTo}${selection}${changeTo}`;
    if (end - begin === 0) {
      end = end + changeTo.length;
    } else {
      end = end + 2 * changeTo.length;
    }
    return [selection, end];
  }

  private changeHeader(selection: string, end: number): [string, number] {
    selection = `${CHANGE_TO.HEADER}${selection}`;
    end = end + CHANGE_TO.HEADER.length;
    return [selection, end];
  }

  private changeCodeBlock(
    before: string,
    selection: string,
    after: string,
    end: number,
  ): [string, string, string, number] {
    const chars = CHANGE_TO.CODE_BLOCK.length + 1; // +1 for the \n
    const changeBefore = `${CHANGE_TO.CODE_BLOCK}\n`;
    const changeAfter = `\n${CHANGE_TO.CODE_BLOCK}`;
    if (before.endsWith(changeBefore) && after.startsWith(changeAfter)) {
      before = before.slice(0, -chars);
      after = after.slice(chars);
    } else {
      selection = ''.concat(changeBefore, selection, changeAfter);
      end = end + chars;
    }
    return [before, selection, after, end];
  }

  private changeList(
    before: string,
    selection: string,
    after: string,
    end: number,
    changeTo: CHANGE_TO.ORDERED_LIST | CHANGE_TO.UNORDERED_LIST,
  ): [string, string, string, number] {
    /*
     * possible cases:
     * 1 undo list
     * 2 swap list type,
     * 3 new list,
     */

    const previous = this.listType[this.listType.length - 1];
    this.listIndentLevel++;
    this.listType.push(changeTo);
    let change = this.SPACES.repeat(this.listIndentLevel) + changeTo;
    if (!before.endsWith('\n')) {
      change = '\n' + change;
    }
    if (before.endsWith(previous)) {
      const previousChange = this.SPACES.repeat(this.listIndentLevel - 1) + previous + '\n';
      const chars = previousChange.length;
      before = before.slice(0, -chars);
    }

    selection = change + selection;
    end = end + change.length;
    return [before, selection, after, end];
  }

  listItemNext(before: string, selection: string, end: number): [string, string, number] {
    if (this.listIndentLevel >= 0) {
      let next;
      const listType = this.listType[this.listType.length - 1];
      const spaces = this.SPACES.repeat(this.listIndentLevel);
      const previous = `${listType}\n`;

      //check if we add an item or exist current list
      if (before.endsWith(previous)) {
        //we must exist current list
        this.listIndentLevel--;
        this.listType = this.listType.slice(0, -1);
        const chars = (spaces + listType + ' \n').length;
        before = before.slice(0, -chars) + '\n';
        if (this.listIndentLevel === -1) {
          before = before + '\n'; // blank line to separate from the list
          end++;
        }
        return this.listItemNext(before, selection, end);
      } else {
        next = spaces + listType;
      }

      selection = selection + next;
      end = end + next.length;
    }
    return [before, selection, end];
  }
  addQuote(selection: string, end: number): [string, number] {
    let selectionLines = selection.split('\n');
    selectionLines = selectionLines.map((line) => CHANGE_TO.QUOTE + line + '\n');
    selection = ''.concat(...selectionLines);
    end = end + CHANGE_TO.QUOTE.length * selectionLines.length;
    return [selection, end];
  }
}

