import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlogEditComponent } from './blog-edit/blog-edit.component';
import { BlogListComponent } from './blog-list/blog-list.component';
import { BlogDetailComponent } from './blog-detail/blog-detail.component';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { MarkdownModule } from 'ngx-markdown';
import { BlogPreviewComponent } from './blog-preview/blog-preview.component';

const routes: Routes = [
  { path: 'new', component: BlogEditComponent },
  { path: ':id/edit', component: BlogEditComponent },
  { path: ':id', component: BlogDetailComponent },
  { path: 'list', component: BlogListComponent },
  { path: '', pathMatch: 'full', component: BlogListComponent },
];

@NgModule({
  declarations: [BlogEditComponent, BlogListComponent, BlogDetailComponent, BlogPreviewComponent],
  imports: [CommonModule, RouterModule.forChild(routes), ReactiveFormsModule, MarkdownModule.forChild()],
})
export class BlogModule {}

