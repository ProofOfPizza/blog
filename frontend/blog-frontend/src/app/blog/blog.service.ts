import { Injectable } from '@angular/core';
import { Blog, BlogApiService } from 'build/openapi';
import { BlogSearchCriteria } from 'build/openapi/model/blogSearchCriteria';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class BlogService {
  constructor(private blogApiService: BlogApiService) {}

  getBlogById(id: string): Observable<Blog> {
    return this.blogApiService.getBlogById(id);
  }

  deleteBlog(id: string): Observable<Blog> {
    return this.blogApiService.deleteBlog(id);
  }

  createBlog(blog: Blog): Observable<Blog> {
    return this.blogApiService.createBlog(blog);
  }

  updateBlog(blog: Blog): Observable<Blog> {
    return this.blogApiService.updateBlog(blog);
  }

  searchBlogs(blogSearchCriteria: BlogSearchCriteria): Observable<Array<Blog>> {
    return this.blogApiService.searchBlogs(blogSearchCriteria);
  }
}

