import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Blog } from 'build/openapi';
import { BlogSearchCriteria } from 'build/openapi/model/blogSearchCriteria';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';
import { BlogService } from '../blog.service';

@Component({
  selector: 'app-blog-list',
  templateUrl: './blog-list.component.html',
  styleUrls: ['./blog-list.component.scss'],
})
export class BlogListComponent implements OnInit {
  blogList$: Observable<Array<Blog>> = new Observable<Array<Blog>>();
  keyUp$: Subject<string> = new Subject<string>();
  @ViewChild('searchAuthor') searchAuthor!: ElementRef;
  @ViewChild('searchText') searchText!: ElementRef;

  constructor(private blogService: BlogService) {}

  ngOnInit(): void {
    this.blogList$ = this.keyUp$.pipe(
      tap(console.log),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(() => this.blogService.searchBlogs(this.getSearchObject())),
    );
    setTimeout(() => {
      this.keyUp$.next('initial loading');
      setTimeout(() => {
        this.keyUp$.next('');
      }, 650);
    });
  }

  private getSearchObject(): BlogSearchCriteria {
    let searchObject: BlogSearchCriteria = {};
    const author = this.searchAuthor.nativeElement.value;
    const text = this.searchText.nativeElement.value;

    if (author.length >= 3) {
      searchObject = { author };
    }
    if (text.length >= 3) {
      searchObject = { ...searchObject, text };
    }
    return searchObject;
  }
}

