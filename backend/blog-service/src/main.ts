import * as config from 'config';
import { AppModule } from './app.module';
import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';

async function bootstrap() {
  const serverConfig = config.get('server');
  // uses env var: NODE_ENV, by default is undefined and the config takes 'development'
  const port = process.env.PORT || serverConfig['port'];
  const logger = new Logger('bootstrap'); //bootstrap is the context
  const app = await NestFactory.create(AppModule, {
    logger: ['log', 'error', 'warn', 'debug', 'verbose'],
  });

  logger.log(process.env.NODE_ENV);
  if (process.env.NODE_ENV === 'development') {
    app.enableCors();
    logger.log('enabled cors in development');
  }
  await app.listen(port);
  logger.log(`Application listening on port ${port}`);
}
bootstrap();
