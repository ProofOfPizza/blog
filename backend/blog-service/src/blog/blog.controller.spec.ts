import { Test, TestingModule } from '@nestjs/testing';
import { NotFoundException } from '@nestjs/common';

import { BlogController } from './blog.controller';
import { BlogService } from './blog.service';

describe('BlogController', () => {
  let blogController: BlogController;
  let blogService: BlogService;
  const mockDto: any = {
    author: 'Kurt Vonnegut',
    title: 'slaughter house 5',
    content:
      'This is a blogpost about unit testing. We use jest for unit tests in the nestJS backend.',
    tags: [],
  };
  const mockBlog = { ...mockDto, id: '12345AA' };

  const mockBlogService = {
    createBlog: jest.fn(),
    getBlogById: jest.fn(),
    searchBlogs: jest.fn(),
    updateBlog: jest.fn(),
    deleteBlog: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BlogController],
      providers: [{ provide: BlogService, useFactory: () => mockBlogService }],
    }).compile();

    blogController = module.get<BlogController>(BlogController);
    blogService = module.get<BlogService>(BlogService);
  });

  it('should be defined', () => {
    expect(blogController).toBeDefined();
  });

  describe('createBlog', () => {
    it('calls the service and returns the created blog', async () => {
      (blogService.createBlog as jest.Mock).mockResolvedValue(mockBlog);
      const result = await blogController.createBlog(mockDto);
      expect(blogService.createBlog).toHaveBeenCalledWith(mockDto);
      expect(result).toEqual(mockBlog);
    });
  });

  describe('searchBlog', () => {
    const mockSearchDto = {
      text: 'abc',
      tag: 'books',
      author: 'Kurt Vonnegut',
    };

    it('retrieves an array of blogs if match is found', async () => {
      (blogService.searchBlogs as jest.Mock).mockResolvedValue([mockBlog]);
      const result = await blogController.searchBlogs(mockSearchDto);
      expect(blogService.searchBlogs).toHaveBeenCalledWith(mockSearchDto);
      expect(result[0]).toEqual(mockBlog);
      expect(result[0].content).toContain('We use jest');
    });

    it('retrieves an empty array if no match is found', async () => {
      (blogService.searchBlogs as jest.Mock).mockResolvedValue([]);
      const result = await blogController.searchBlogs(mockSearchDto);
      expect(blogService.searchBlogs).toHaveBeenCalledWith(mockSearchDto);
      expect(result).toEqual([]);
    });
  });

  describe('getBlogById', () => {
    it('should retrieve a blog when id is found', async () => {
      (blogService.getBlogById as jest.Mock).mockResolvedValue(mockBlog);
      const result = await blogController.getBlogById('id-1');
      expect(blogService.getBlogById).toHaveBeenCalledWith('id-1');
      expect(result).toEqual(mockBlog);
    });

    it('should throw a 404 exception if the id is not found', async () => {
      (blogService.getBlogById as jest.Mock).mockImplementation(() => {
        throw new NotFoundException();
      });
      expect(blogController.getBlogById('false-id')).rejects.toThrowError(
        NotFoundException,
      );
    });
  });

  describe('updateBlog', () => {
    const updatedBlog = { ...mockDto, author: 'Jimi Hendrix' };

    it('updates the blog to new value and returns that', async () => {
      (blogService.updateBlog as jest.Mock).mockResolvedValue(updatedBlog);
      const result = await blogController.updateBlog(mockDto);
      expect(blogService.updateBlog).toHaveBeenCalledWith(mockDto);
      expect(result).toEqual(updatedBlog);
    });

    it('should throw a 404 exception if the id is not found', async () => {
      (blogService.updateBlog as jest.Mock).mockImplementation(() => {
        throw new NotFoundException();
      });
      expect(blogController.updateBlog(mockDto)).rejects.toThrowError(
        NotFoundException,
      );
    });
  });

  describe('deleteBlog', () => {
    it('should delete a blog when id is found', async () => {
      (blogService.deleteBlog as jest.Mock).mockResolvedValue(mockBlog);
      const result = await blogController.deleteBlog('id-1');
      expect(blogService.deleteBlog).toHaveBeenCalledWith('id-1');
      expect(result).toEqual(mockBlog);
    });

    it('should throw a 404 exception if the id is not found', async () => {
      (blogService.deleteBlog as jest.Mock).mockImplementation(() => {
        throw new NotFoundException();
      });
      expect(blogController.deleteBlog('false-id')).rejects.toThrowError(
        NotFoundException,
      );
    });
  });
});
