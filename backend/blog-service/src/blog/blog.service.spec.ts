import { Test, TestingModule } from '@nestjs/testing';
import { NotFoundException } from '@nestjs/common';

import { BlogRepository } from './blog.repository';
import { BlogService } from './blog.service';

describe('BlogService', () => {
  let blogService: BlogService;
  let blogRepository: BlogRepository;
  const mockDto: any = {
    author: 'Kurt Vonnegut',
    title: 'slaughter house 5',
    content:
      'This is a blogpost about unit testing. We use jest for unit tests in the nestJS backend.',
    tags: [],
  };
  const mockBlog = { ...mockDto, id: '12345AA' };

  const mockBlogRepository = {
    createBlog: jest.fn(),
    searchBlogs: jest.fn(),
    findOne: jest.fn(),
    updateBlog: jest.fn(),
    remove: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BlogService,
        { provide: BlogRepository, useFactory: () => mockBlogRepository },
      ],
    }).compile();

    blogService = module.get<BlogService>(BlogService);
    blogRepository = module.get<BlogRepository>(BlogRepository);
  });

  it('should be defined', () => {
    expect(blogService).toBeDefined();
  });

  describe('createBlog', () => {
    it('should create a blog with valid input', async () => {
      (blogRepository.createBlog as jest.Mock).mockResolvedValue(mockBlog);
      const result = await blogService.createBlog(mockDto);
      expect(blogRepository.createBlog).toHaveBeenCalledWith(mockDto);
      expect(result).toEqual(mockBlog);
    });
  });

  describe('searchBlog', () => {
    const mockSearchDto = {
      text: 'abc',
      tag: 'books',
      author: 'Kurt Vonnegut',
    };

    it('retrieves an array of blogs if match is found', async () => {
      (blogRepository.searchBlogs as jest.Mock).mockResolvedValue([mockBlog]);
      const result = await blogService.searchBlogs(mockSearchDto);
      expect(blogRepository.searchBlogs).toHaveBeenCalledWith(mockSearchDto);
      expect(result[0]).toEqual(mockBlog);
      expect(result[0].content).toContain('We use jest');
    });

    it('retrieves an empty array if no match is found', async () => {
      (blogRepository.searchBlogs as jest.Mock).mockResolvedValue([]);
      const result = await blogService.searchBlogs(mockDto);
      expect(blogRepository.searchBlogs).toHaveBeenCalledWith(mockDto);
      expect(result).toEqual([]);
    });
  });

  describe('getBlogById', () => {
    it('returns a blog when id is found', async () => {
      (blogRepository.findOne as jest.Mock).mockResolvedValue(mockBlog);
      const result = await blogService.getBlogById('1234B');
      expect(blogRepository.findOne).toHaveBeenCalledWith({
        where: { id: '1234B' },
      });
      expect(result).toEqual(mockBlog);
    });

    it('thows 404 exception if the id is not found', async () => {
      (blogRepository.findOne as jest.Mock).mockResolvedValue(null);
      expect(blogService.getBlogById('false-id')).rejects.toThrowError(
        NotFoundException,
      );
    });
  });

  describe('updateBlog', () => {
    it('updates a blog and returns that', async () => {
      const mockUpdateDto = { ...mockDto, id: '1223BB' };
      (blogRepository.findOne as jest.Mock).mockResolvedValue(mockBlog);
      (blogRepository.updateBlog as jest.Mock).mockResolvedValue(mockUpdateDto);
      const result = await blogService.updateBlog(mockUpdateDto);
      expect(blogRepository.updateBlog).toHaveBeenCalledWith(mockUpdateDto);
      expect(result).toEqual(mockUpdateDto);
    });

    it('thows 404 exception if the id is not found', async () => {
      (blogRepository.findOne as jest.Mock).mockResolvedValue(null);
      expect(blogService.updateBlog(mockDto)).rejects.toThrowError(
        NotFoundException,
      );
    });
  });
  describe('deleteBlog', () => {
    it('deletes a blog', async () => {
      (blogRepository.remove as jest.Mock).mockResolvedValue(mockBlog);
      blogService.getBlogById = jest.fn().mockResolvedValue(mockBlog);
      const result = await blogService.deleteBlog('id-1');
      expect(blogService.getBlogById).toHaveBeenCalledWith('id-1');
      expect(blogRepository.remove).toHaveBeenCalledWith(mockBlog);
      expect(result).toEqual(mockBlog);
    });

    it('thows 404 exception if the id is not found', async () => {
      (blogRepository.findOne as jest.Mock).mockResolvedValue(null);
      expect(blogService.updateBlog(mockDto)).rejects.toThrowError(
        NotFoundException,
      );
    });
  });
});
