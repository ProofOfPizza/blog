import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { Blog } from './blog.entity';
import { BlogService } from './blog.service';
import { CreateBlogDto } from './dto/create-blog.dto';
import { SearchBlogsDto } from './dto/search-blogs.dto';
import { UpdateBlogDto } from './dto/update-blog-dto';

@Controller('api/blog')
export class BlogController {
  constructor(private blogService: BlogService) {}

  @Get(':id')
  async getBlogById(@Param('id') id: string): Promise<Blog> {
    return this.blogService.getBlogById(id);
  }

  @Delete(':id')
  async deleteBlog(@Param('id') id: string): Promise<Blog> {
    return this.blogService.deleteBlog(id);
  }

  @Post('/')
  @UsePipes(ValidationPipe)
  async createBlog(@Body() createBlogDto: CreateBlogDto): Promise<Blog> {
    return this.blogService.createBlog(createBlogDto);
  }

  @Put('/')
  @UsePipes(ValidationPipe)
  async updateBlog(@Body() updateBlogDto: UpdateBlogDto): Promise<Blog> {
    return this.blogService.updateBlog(updateBlogDto);
  }

  @Post('search')
  @UsePipes(ValidationPipe)
  async searchBlogs(
    @Body() searchBlogsDto: SearchBlogsDto,
  ): Promise<Array<Blog>> {
    return this.blogService.searchBlogs(searchBlogsDto);
  }
}

