import { IsOptional, MinLength } from 'class-validator';
export class SearchBlogsDto {
  @IsOptional()
  @MinLength(3)
  author: string;
  @IsOptional()
  @MinLength(3)
  text: string;
  @IsOptional()
  @MinLength(3)
  tag: string;
}
