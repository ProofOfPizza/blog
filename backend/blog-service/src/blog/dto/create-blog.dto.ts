import { MinLength } from 'class-validator';

export class CreateBlogDto {
  @MinLength(3)
  author: string;
  @MinLength(8)
  title: string;
  @MinLength(50)
  content: string;
  @MinLength(3, { each: true })
  tags: Array<string>;
}
