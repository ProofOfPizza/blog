import { IsString, MinLength } from 'class-validator';

export class UpdateBlogDto {
  @IsString()
  id: string;

  @IsString()
  @MinLength(3)
  author: string;

  @IsString()
  @MinLength(8)
  title: string;

  @IsString()
  @MinLength(50)
  content: string;

  @IsString()
  date: string;

  @IsString({ each: true })
  @MinLength(3, { each: true })
  tags: Array<string>;
}
