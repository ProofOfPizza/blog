import * as day from 'dayjs';
import { EntityRepository, MongoRepository } from 'typeorm';
import { InternalServerErrorException, Logger } from '@nestjs/common';
import { v4 as uuid } from 'uuid';

import { Blog } from './blog.entity';
import { CreateBlogDto } from './dto/create-blog.dto';
import { SearchBlogsDto } from './dto/search-blogs.dto';
import { UpdateBlogDto } from './dto/update-blog-dto';

@EntityRepository(Blog)
export class BlogRepository extends MongoRepository<Blog> {
  private logger = new Logger('BlogRepository', true); // not sure what true does...

  async createBlog(createBlogDto: CreateBlogDto): Promise<Blog> {
    const blog = this.create({
      ...createBlogDto,
      id: uuid(),
      date: day().format('MMMM, DD YYYY'),
    });
    return blog.save();
  }

  async updateBlog(updateBlogDto: UpdateBlogDto, found: Blog): Promise<Blog> {
    const blog = this.create({ ...found, ...updateBlogDto });
    return blog.save();
  }

  async searchBlogs(searchBlogsDto: SearchBlogsDto): Promise<Array<Blog>> {
    const { author, text, tag } = searchBlogsDto;
    const andQuery = [];
    const orQuery = [];
    const query = {};

    if (text) {
      orQuery.push({ title: new RegExp(text, 'i') });
      orQuery.push({ content: new RegExp(text, 'i') });
      query['$or'] = orQuery;
      this.logger.debug(`added or to query: ${JSON.stringify(query)}`);
    }

    if (author) {
      andQuery.push({ author: new RegExp(author, 'i') });
      query['$and'] = andQuery;
      this.logger.debug(`added and to query: ${JSON.stringify(query)}`);
    }

    if (tag) {
      andQuery.push({ tags: tag });
      query['$and'] = andQuery;
      this.logger.debug(`added and to query: ${JSON.stringify(query)}`);
    }

    try {
      const blogs = this.find({
        where: query,
      });
      return blogs;
    } catch (error) {
      this.logger.error(
        `Failed to get blogs for user DTO: ${JSON.stringify(searchBlogsDto)}`,
        error.stack,
      );
      throw new InternalServerErrorException();
    }
  }
}

