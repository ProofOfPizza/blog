import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { Blog } from './blog.entity';
import { BlogRepository } from './blog.repository';
import { CreateBlogDto } from './dto/create-blog.dto';
import { SearchBlogsDto } from './dto/search-blogs.dto';
import { UpdateBlogDto } from './dto/update-blog-dto';

@Injectable()
export class BlogService {
  private logger = new Logger('BlogService', true);
  constructor(
    @InjectRepository(BlogRepository) private blogRepository: BlogRepository,
  ) {}

  async getBlogById(id: string): Promise<Blog> {
    this.logger.debug(`getBlogById called with id: ${id}`);
    const found = await this.blogRepository.findOne({ where: { id } });
    if (!found) {
      throw new NotFoundException(`Blog with id ${id} is not found.`);
    }
    return found;
  }

  async deleteBlog(id: string): Promise<Blog> {
    this.logger.debug(`deleteBlog called with id: ${id}`);
    const blog = await this.getBlogById(id);
    return this.blogRepository.remove(blog);
  }

  async createBlog(createBlogDto: CreateBlogDto): Promise<Blog> {
    this.logger.debug(
      `createBlog called with dto: ${JSON.stringify(createBlogDto)}`,
    );
    return this.blogRepository.createBlog(createBlogDto);
  }

  async updateBlog(updateBlogDto: UpdateBlogDto): Promise<Blog> {
    this.logger.debug(
      `updateBlog called with dto: ${JSON.stringify(updateBlogDto)}`,
    );
    const id = updateBlogDto.id;
    const found = await this.blogRepository.findOne({ where: { id } });
    if (!found) {
      throw new NotFoundException(
        `Update Failed: Blog with id ${id} is not found.`,
      );
    }
    return this.blogRepository.updateBlog(updateBlogDto, found);
  }

  async searchBlogs(searchBlogsDto: SearchBlogsDto): Promise<Array<Blog>> {
    this.logger.debug(
      `searchBlogs called with dto: ${JSON.stringify(searchBlogsDto)}`,
    );
    return this.blogRepository.searchBlogs(searchBlogsDto);
  }
}

