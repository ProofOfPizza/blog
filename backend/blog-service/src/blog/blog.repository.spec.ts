import * as day from 'dayjs';
import { Test, TestingModule } from '@nestjs/testing';

import { Blog } from './blog.entity';
import { BlogRepository } from './blog.repository';

describe('BlogRepository', () => {
  let blogRepository: BlogRepository;

  const mockBlog: Blog = new Blog();

  mockBlog.author = 'Kurt Vonnegut';
  mockBlog.title = 'slaughter house 5';
  mockBlog.content =
    'This is a blogpost about unit testing. We use jest for unit tests in the nestJS backend.';
  mockBlog.tags = [];
  mockBlog.id = 'id-1';
  mockBlog.date = day('2021-04-15').format('MMMM, DD YYYY');

  let mockDto: any;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BlogRepository],
    }).compile();

    blogRepository = module.get<BlogRepository>(BlogRepository);
  });

  it('should be defined', () => {
    expect(blogRepository).toBeDefined();
  });

  describe('createBlog', () => {
    let save: any;
    beforeEach(() => {
      save = jest.fn();
      blogRepository.create = jest.fn().mockReturnValue({ save });
    });

    it('creates a blog in db', async () => {
      mockDto = mockBlog;
      save.mockResolvedValue(mockBlog);
      const result = await blogRepository.createBlog(mockDto);
      expect(save).toHaveBeenCalled();
      expect(result).toEqual(mockBlog);
      //date in correct format
      expect(result.date).toEqual('April, 15 2021');
    });

    describe('searchBlog', () => {
      beforeEach(() => {
        blogRepository.find = jest.fn();
      });
      it('aggregates all search parameters', async () => {
        mockDto = {
          author: 'Kurt',
          text: 'values of object do not matter',
          tag: 'books',
        };
        await blogRepository.searchBlogs(mockDto);
        expect(blogRepository.find).toHaveBeenCalledWith({
          where: {
            $and: [
              {
                author: 'Kurt',
              },
              {
                tags: 'books',
              },
            ],
            $or: [
              {
                title: /values of object do not matter/,
              },
              {
                content: /values of object do not matter/,
              },
            ],
          },
        });
      });
    });

    describe('updateBlog', () => {
      let save: any;
      beforeEach(() => {
        save = jest.fn();
        blogRepository.create = jest.fn().mockReturnValue({ save });
      });

      it('updates a blog in db', async () => {
        mockDto = mockBlog;
        save.mockResolvedValue(mockBlog);
        const result = await blogRepository.updateBlog(mockDto);
        expect(save).toHaveBeenCalled();
        expect(result).toEqual(mockBlog);
        //date in correct format
        expect(result.date).toEqual('April, 15 2021');
      });
    });
  });
});
