import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { BlogModule } from './blog/blog.module';
import { typeOrmConfig } from './config/typeorm.config';

@Module({
  imports: [BlogModule, TypeOrmModule.forRoot(typeOrmConfig)],
})
export class AppModule {}
