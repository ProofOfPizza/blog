import * as config from 'config';
import { TypeOrmModuleOptions } from '@nestjs/typeorm';

const dbConfig = config.get('db');

const createUrl: () => string = () =>
  `mongodb+srv://${dbConfig['username']}:${dbConfig['password']}@cluster0.fdx56.mongodb.net/${dbConfig['database']}?retryWrites=true&w=majority`;

export const typeOrmConfig: TypeOrmModuleOptions = {
  type: dbConfig['type'],
  url: createUrl(),
  entities: [__dirname + '../**/*.entity.ts'],
  autoLoadEntities: true,
  synchronize: process.env.TYPEORM_SYNC || dbConfig['synchronize'],
  useUnifiedTopology: true,
  useNewUrlParser: true,
};
