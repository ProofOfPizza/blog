{ pkgs ? import <nixos-unstable> {} }:

pkgs.mkShell {
  buildInputs = with pkgs; [
    openapi-generator-cli
    awscli2
    python27
    nodejs-14_x
    yarn
    sass
    mongodb-compass
  ];

  permittedInsecurePackages = [
    "openssl-1.0.2u"
  ];

}
